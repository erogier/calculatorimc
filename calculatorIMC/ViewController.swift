//
//  ViewController.swift
//  calculatorIMC
//
//  Created by Eddy Rogier on 09/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tailleTextField: UITextField!
    @IBOutlet weak var poidsTextField: UITextField!
    var valeurTaille:Int?
    var valeurPoids:Int?
    var imc:Double?
    @IBOutlet weak var valueImc: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    // MARK: - action taille et poids
    @IBAction func tailleStepper(_ sender: UIStepper) {
        
        valeurTaille = Int(sender.value)
        tailleTextField.text = "\(valeurTaille!) cm"

    }
    
    @IBAction func poidStepper(_ sender: UIStepper) {
        valeurPoids = Int(sender.value)
        poidsTextField.text  = "\(valeurPoids!) kg"
    }
    
    
    // MARK: - button
    @IBAction func calculerIMCAction(_ sender: UIButton) {
        print("calcule")
        if (valeurTaille == nil) || valeurPoids == nil {
          print("ʕ•ᴥ•ʔ saisir une valeur")
        } else {
            print("calcule imc")
            calculeImc()
        }
    }
    
    
    func calculeImc(){
        let taille2 = Double(valeurTaille!)/100 * Double(valeurTaille!)/100
        imc = Double(valeurPoids!) / taille2
        //print("ʕ•ᴥ•ʔ resultat imc \(imc)")
        interpreterIMC(imc: imc!)
    }
    
    
    
    
    func interpreterIMC(imc:Double) -> String {
        
        var text = " avec un IMC de \(round(imc))"
    
        switch imc {
        case 0...16:
            valueImc.textColor = UIColor.gray
            valueImc.text = "Dénutrition \(text)"
            
        case 16.5...18.5:
            valueImc.textColor = UIColor.blue
            valueImc.text = "État de maigreur\(text)"
            
        case 18.5...25:
            valueImc.textColor = UIColor.green
            valueImc.text = "Corpulence normale\(text)"
            
        case 25...30:
            valueImc.textColor = UIColor.orange
            valueImc.text = "Surpoids\(text)"
            
        case 30...35:
            valueImc.textColor = UIColor.brown
            valueImc.text = "Obésité modéré\(text)"
            
        case 35...40:
            valueImc.textColor = UIColor.red
            valueImc.text = "Obèsité sévère\(text)"
            
        case 40...50:
            valueImc.textColor = UIColor.black
            valueImc.text = "Obèsité morbide\(text)"
            
        default:
            print("")
        }
        return text
    }
    
    
    
    
   
    
}

